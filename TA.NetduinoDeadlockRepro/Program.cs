﻿// This file is part of the TA.NetduinoDeadlockRepro project
// 
// Copyright © 2015 Tigra Networks., all rights reserved.
// 
// File: Program.cs  Last modified: 2015-08-06@05:04 by Tim Long

using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Net.NetworkInformation;
using SecretLabs.NETMF.Hardware.Netduino;

namespace TA.NetduinoDeadlockRepro
    {
    public class Program
        {
        static readonly ManualResetEvent networkAvailableEvent = new ManualResetEvent(false);

        public static void Main()
            {
            WaitForNetwork();
            SetSystemClock();
            var pingpong1 = new LedPingpong(Pins.GPIO_PIN_D8, Pins.GPIO_PIN_D9);
            pingpong1.StartPingPong();
            Thread.Sleep(500);
            var pingpong2 = new LedPingpong(Pins.GPIO_PIN_D10, Pins.GPIO_PIN_D11);
            pingpong2.StartPingPong();

            Thread.Sleep(-1);
            }

        static void SetSystemClock()
            {
            var utc = NetworkTime.GetNetworkTimeUtc();
            Utility.SetLocalTime(utc);
            Dbg.Trace("Set system time to: " + utc + " UTC", Source.Unspecified);
            }

        static void WaitForNetwork()
            {
            NetworkChange.NetworkAvailabilityChanged += HandleNetworkAvailabilityChanged;
            networkAvailableEvent.WaitOne();
            WaitForValidIpAddress();
            }

        static void HandleNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
            {
            if (e.IsAvailable)
                {
                Debug.Print("ConnectionHandler available");
                networkAvailableEvent.Set();
                }
            else
                {
                Debug.Print("ConnectionHandler down");
                networkAvailableEvent.Reset();
                }
            }

        static void WaitForValidIpAddress()
            {
            NetworkInterface[] networkInterfaces;
            Dbg.Trace("Waiting for valid IP address", Source.NetworkServer);
            do
                {
                Thread.Sleep(100);
                networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                } while (networkInterfaces[0].IPAddress == "0.0.0.0");

            Dbg.Trace("Found " + networkInterfaces.Length + " network interfaces - details as follows...",
                Source.NetworkServer);
            for (int i = 0; i < networkInterfaces.Length; i++)
                {
                var nic = networkInterfaces[i];
                Dbg.Trace("Network interface: " + i, Source.NetworkServer);
                Dbg.Trace("  Interface type: " + nic.NetworkInterfaceType, Source.NetworkServer);
                Dbg.Trace("  IPv4 address: " + nic.IPAddress, Source.NetworkServer);
                Dbg.Trace("  Default gateway: " + nic.GatewayAddress, Source.NetworkServer);
                Dbg.Trace("  Subnet mask: " + nic.SubnetMask, Source.NetworkServer);

                var dnsServers = nic.DnsAddresses;
                Dbg.Trace("  DNS servers:", Source.NetworkServer);
                foreach (var dnsServer in dnsServers)
                    {
                    Dbg.Trace("    " + dnsServer, Source.NetworkServer);
                    }
                }
            }
        }
    }