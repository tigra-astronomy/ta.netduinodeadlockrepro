// This file is part of the TA.NetduinoDeadlockRepro project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: LedPingPong.cs  Last modified: 2015-08-06@05:09 by Tim Long

using System;
using System.Threading;
using Microsoft.SPOT.Hardware;

namespace TA.NetduinoDeadlockRepro
    {
    internal class LedPingpong
        {
        readonly OutputPort ledPing;
        readonly OutputPort ledPong;
        static readonly Timeout LedSleepTime = Timeout.FromMilliseconds(999);
        readonly AutoResetEvent PingPongEvent = new AutoResetEvent(false);
        Thread Ping;
        Thread Pong;

        public LedPingpong(Cpu.Pin ping, Cpu.Pin pong)
            {
            ledPing = new OutputPort(ping, false);
            ledPong = new OutputPort(pong, false);
            }

        void PingThread()
            {
            while (true)
                {
                ledPing.Write(true);
                Thread.Sleep(LedSleepTime);
                ledPing.Write(false);
                PingPongEvent.Set();
                PingPongEvent.WaitOne();
                Dbg.Trace(DateTime.UtcNow.ToString(), Source.PingPong);
                }
            }

        void PongThread()
            {
            while (true)
                {
                ledPong.Write(true);
                Thread.Sleep(LedSleepTime);
                ledPong.Write(false);
                PingPongEvent.Set();
                PingPongEvent.WaitOne();
                }
            }

        public void StartPingPong()
            {
            Ping = new Thread(PingThread);
            Ping.Start();
            PingPongEvent.WaitOne();
            Pong = new Thread(PongThread);
            Pong.Start();
            }
        }
    }