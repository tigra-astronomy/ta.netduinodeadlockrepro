// This file is part of the TA.NetduinoDeadlockRepro project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: Timeout.cs  Last modified: 2015-08-06@05:04 by Tim Long

using System;

namespace TA.NetduinoDeadlockRepro
    {
    /// <summary>
    ///     Encapsulates the concept of a time-out as a reference type, so that it can be defaulted to null in optional
    ///     parameters.
    /// </summary>
    public struct Timeout
        {
        /// <summary>
        ///     Gets the timeout as a <see cref="TimeSpan" />.
        /// </summary>
        public TimeSpan TimeSpan { get; private set; }

        /// <summary>
        ///     Gets the timeout expressed in milliseconds.
        /// </summary>
        public int Milliseconds
            {
            get { return (int) (TimeSpan.Ticks / TimeSpan.TicksPerMillisecond); }
            }

        /// <summary>
        ///     Gets the timeout expressed in system clock ticks.
        /// </summary>
        public long Ticks
            {
            get { return TimeSpan.Ticks; }
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class froma a <see cref="TimeSpan" />.
        /// </summary>
        /// <param name="duration">The timeout duration.</param>
        Timeout(TimeSpan duration) : this()
            {
            TimeSpan = duration;
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class from the specified number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The timeout duration, in milliseconds.</param>
        Timeout(int milliseconds) : this()
            {
            TimeSpan = TimeSpan.FromTicks(milliseconds * TimeSpan.TicksPerMillisecond);
            }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Timeout" /> class from a number of clock ticks.
        /// </summary>
        /// <param name="ticks">The timeout duration in system clock ticks.</param>
        Timeout(long ticks) : this()
            {
            TimeSpan = TimeSpan.FromTicks(ticks);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of milliseconds.
        /// </summary>
        /// <param name="milliseconds">The timeout interval, in whole milliseconds.</param>
        public static Timeout FromMilliseconds(int milliseconds)
            {
            return new Timeout(milliseconds);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of system clock ticks.
        /// </summary>
        /// <param name="ticks">The ticks.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromTicks(long ticks)
            {
            return new Timeout(ticks);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified interval.
        /// </summary>
        /// <param name="span">The timeout duration, as a <see cref="TimeSpan" />.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromTimeSpam(TimeSpan span)
            {
            return new Timeout(span);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of whole seconds.
        /// </summary>
        /// <param name="seconds">The timeout interval, in seconds.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromSeconds(int seconds)
            {
            return FromMilliseconds(1000 * seconds);
            }

        /// <summary>
        ///     Gets a <see cref="Timeout" /> set to the specified number of fractional seconds.
        /// </summary>
        /// <param name="seconds">The timeout period, in seconds.</param>
        /// <returns>Timeout.</returns>
        public static Timeout FromSeconds(double seconds)
            {
            return FromTicks((long) (TimeSpan.TicksPerSecond * seconds));
            }


        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="TimeSpan" />.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>The timeout value converted to a <see cref="TimeSpan" />.</returns>
        public static implicit operator TimeSpan(Timeout t)
            {
            return t.TimeSpan;
            }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="System.Int32" />.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>The timeout value converted to milliseconds.</returns>
        public static implicit operator int(Timeout t)
            {
            return t.Milliseconds;
            }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="Timeout" /> to <see cref="System.Int64" />.
        /// </summary>
        /// <param name="t">The timeout instance.</param>
        /// <returns>The timeout value expressed in system clock ticks.</returns>
        public static implicit operator long(Timeout t)
            {
            return t.Ticks;
            }

        public override string ToString()
            {
            return TimeSpan.ToString();
            }
        }
    }