// This file is part of the TA.NetduinoDeadlockRepro project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: NetworkTime.cs  Last modified: 2015-08-06@05:04 by Tim Long

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TA.NetduinoDeadlockRepro
    {
    internal static class NetworkTime
        {
        internal static DateTime GetNetworkTimeUtc()
            {
            /*** GET TIME EXAMPLE START ***/

            // resolve DNS address
            /* NOTE: choose any of the following time servers */
            //string SNTPserver = "nist1-nj2.ustiming.org";
            //string SNTPserver = "nist1-pa.ustiming.org";
            //string SNTPserver = "time-a.nist.gov";
            //string SNTPserver = "time-b.nist.gov";
            //string SNTPserver = "time-c.nist.gov";
            //string SNTPserver = "ntp-nist.ldsbc.edu";
            //string SNTPserver = "nist1-macon.macon.ga.us";
            string SNTPserver = "pool.ntp.org";
            Dbg.Trace("Attempting to st the time from " + SNTPserver, Source.NetTime);
            IPHostEntry ipHostEntry;
            IPAddress ipAddress;
            IPEndPoint ipEndPoint;

            ipHostEntry = Dns.GetHostEntry(SNTPserver);
            ipAddress = ipHostEntry.AddressList[0];
            ipEndPoint = new IPEndPoint(ipAddress, 0x7B);
            Dbg.Trace("Resolved IP Address: " + ipAddress, Source.NetTime);

            DateTime currentUtcDateTime;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                {
                byte[] dataBuf = new byte[48];
                dataBuf[0] = 0x1B;
                int bytesSent = socket.SendTo(dataBuf, ipEndPoint);
                if (bytesSent != dataBuf.Length)
                    {
                    Dbg.Trace("Failed sending SNTP request.", Source.NetTime);
                    throw new IOException("Unable to send SNTP request");
                    }
                EndPoint refEndPoint = ipEndPoint;
                socket.ReceiveFrom(dataBuf, ref refEndPoint);
                Dbg.Trace("Network time received", Source.NetTime);
                int index = 40;
                UInt32 seconds = (
                    ((UInt32) dataBuf[index] << 24) |
                    ((UInt32) dataBuf[index + 1] << 16) |
                    ((UInt32) dataBuf[index + 2] << 8) |
                    dataBuf[index + 3]
                    );
                index += sizeof (UInt32);
                UInt32 fractionalSeconds = (
                    ((UInt32) dataBuf[index] << 24) |
                    ((UInt32) dataBuf[index + 1] << 16) |
                    ((UInt32) dataBuf[index + 2] << 8) |
                    dataBuf[index + 3]
                    );
                Int64 totalMilliseconds = (seconds * (Int64) 1000) +
                                          (Int64) ((((double) fractionalSeconds / UInt32.MaxValue) * 1000));
                // SNTP's seconds are measured as # of seconds since midnight on january 1, 1900.
                currentUtcDateTime = DateTime.SpecifyKind(new DateTime(1900, 1, 1), DateTimeKind.Utc);
                currentUtcDateTime =
                    currentUtcDateTime.Add(TimeSpan.FromTicks(totalMilliseconds * TimeSpan.TicksPerMillisecond));
                Dbg.Trace("UTC Time: " + currentUtcDateTime, Source.NetTime);
                }
            return currentUtcDateTime;
            }
        }
    }